using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace tufol.Models
{
    [Table("m_announcement")]
    public class AnnouncementModel
    {
        [MaxLength(50, ErrorMessage = "Maximal 50 Character")]
        public string key_flag { get; set; }

        [MaxLength(100, ErrorMessage = "Maximal 100 Character")]
        public string? title { get; set; }
        public string? content { get; set; }
        [MaxLength(100, ErrorMessage = "Maximal 100 Character")]
        public string? title_en { get; set; }
        public string? content_en { get; set; }
    }
}