# First stage
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /DockerSource

# Copy csproj and restore as distinct layers
#COPY *.sln .
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build website
COPY . ./
WORKDIR /DockerSource
RUN dotnet publish -c release -o /DockerOutput/Website --no-restore

# Final stage
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /DockerOutput/Website
COPY --from=build /DockerOutput/Website ./

EXPOSE 8080
ENTRYPOINT ["dotnet", "tufol.dll"]
